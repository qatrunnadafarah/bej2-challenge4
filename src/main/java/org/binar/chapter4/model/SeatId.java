package org.binar.chapter4.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Embeddable
public class SeatId {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer seatID;
}
