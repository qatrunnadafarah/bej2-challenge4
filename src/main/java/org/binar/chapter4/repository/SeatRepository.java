package org.binar.chapter4.repository;

import org.binar.chapter4.model.Film;
import org.binar.chapter4.model.Seat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.util.List;

@Repository
public interface SeatRepository extends JpaRepository<Seat, Integer> {

    @Query(nativeQuery = true, value = "select * from seat")
    public List<Seat> findAllSeat(Pageable pageable);
}
