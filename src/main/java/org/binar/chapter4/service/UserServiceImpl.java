package org.binar.chapter4.service;

import org.binar.chapter4.model.UserActive;
import org.binar.chapter4.repository.UserActiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserActiveRepository userActiveRepository;

    @Override
    public void addUser(UserActive userActive) {
        userActiveRepository.save(userActive);
    }

    @Override
    public void updateUser(Integer userID, String username, String email, String password) throws Exception {

    }

    @Override
    public void deleteUserById(UserActive userActive) throws Exception {
        userActiveRepository.deleteById(userActive.getUserID());
    }
}
