package org.binar.chapter4.service;

import org.binar.chapter4.model.Film;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FilmService {

    void addFilm (Film film) throws Exception;

    void updateFilm(String title, Integer filmID) throws Exception;

    void deleteFilm(Film film);

    List<Film> getAllFilms();

    void getScheduleByFilmId(String filmId);

}
