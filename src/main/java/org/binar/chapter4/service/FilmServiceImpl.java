package org.binar.chapter4.service;

import org.binar.chapter4.model.Film;
import org.binar.chapter4.repository.FilmRepository;
import org.binar.chapter4.repository.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class FilmServiceImpl implements FilmService {

    @Autowired
    FilmRepository filmRepository;
    ScheduleRepository scheduleRepository;

    @Override
    public void addFilm(Film film) throws Exception {
        filmRepository.save(film);
    }

    @Override
    public void updateFilm(String title, Integer filmID) throws Exception {

    }

    @Override
    public void deleteFilm(Film film) {
        filmRepository.deleteById(film.getFilmID());
    }

    @Override
    public List<Film> getAllFilms() {
        return null;
    }

    @Override
    public void getScheduleByFilmId(String filmId) {

    }
}
