package org.binar.chapter4.repository;

import org.binar.chapter4.model.Film;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class FilmRepositoryTest {

    @Autowired
    FilmRepository filmRepository;

    @Test
    void insertFilm() {
        Film film = new Film();
        film.setTitle("Upin Ipin");
        film.setSchedule("2022-01-02");

        //save data ke table database
        filmRepository.save(film);
    }
}
